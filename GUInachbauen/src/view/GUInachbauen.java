package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

//import sun.font.Font2D;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;

import java.awt.SystemColor;
import javax.swing.UIManager;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class GUInachbauen extends JFrame {

	private JPanel contentPane;
	private JTextField txtTextEingabe;
	private JTextField txtTextZuVeraendern;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUInachbauen frame = new GUInachbauen();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUInachbauen() {
		setBackground(new Color(240, 240, 240));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		// Roter Button
		JButton btnRed = new JButton("Rot");
		btnRed.setBackground(UIManager.getColor("Button.background"));
		btnRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonRed_clicked();
			}
		});
		btnRed.setBounds(10, 108, 117, 21);
		contentPane.add(btnRed);

		// Gr�ner Button
		JButton btnGreen = new JButton("Gr\u00FCn");
		btnGreen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonGreen_clicked();
			}
		});
		btnGreen.setBackground(UIManager.getColor("Button.background"));
		btnGreen.setBounds(137, 108, 162, 21);
		contentPane.add(btnGreen);

		// Blauer Button
		JButton btnBlue = new JButton("Blau");
		btnBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonBlue_clicked();
			}
		});
		btnBlue.setBackground(UIManager.getColor("Button.background"));
		btnBlue.setBounds(309, 108, 117, 21);
		contentPane.add(btnBlue);

		// Gelber Butto
		JButton btnYellow = new JButton("Gelb");
		btnYellow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonYellow_clicked();
			}
		});
		btnYellow.setBackground(UIManager.getColor("Button.background"));
		btnYellow.setBounds(10, 139, 117, 21);
		contentPane.add(btnYellow);

		// Standardfarbe Button
		JButton btnStandard = new JButton("Standardfarbe");
		btnStandard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonStandard_clicked();
			}
		});
		btnStandard.setBackground(UIManager.getColor("Button.background"));
		btnStandard.setBounds(137, 139, 162, 21);
		contentPane.add(btnStandard);

		// Farbe w�hlen Button, der zum jetzigen Zeitpunkt noch nichts kann
		JButton btnFarbeWaehlen = new JButton("Farbe w\u00E4hlen");
		btnFarbeWaehlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonFarbewaehlen_clicked();
			}
		});
		btnFarbeWaehlen.setBackground(UIManager.getColor("Button.background"));
		btnFarbeWaehlen.setBounds(309, 139, 117, 21);
		contentPane.add(btnFarbeWaehlen);

		// Text "Aufgabe1" als Label erstellt
		JLabel lblAufgabe1 = new JLabel("Aufgabe 1:Hintergrundfarbe \u00E4ndern");
		lblAufgabe1.setBounds(10, 85, 317, 13);
		contentPane.add(lblAufgabe1);

		// Text "Aufgabe2" als Label erstellt
		JLabel lblAufgabe2 = new JLabel("Aufgabe 2: Text formatieren");
		lblAufgabe2.setBounds(10, 170, 194, 13);
		contentPane.add(lblAufgabe2);

		// Schriftart "Arial" Button
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonArial_clicked();
			}
		});
		btnArial.setBounds(10, 193, 117, 21);
		contentPane.add(btnArial);

		// Schriftart "Comic Sans MS" Button
		JButton btnComicSansMs = new JButton("Comic Sans MS");
		btnComicSansMs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonComicSansMS_clicked();
			}
		});
		btnComicSansMs.setBounds(137, 193, 162, 21);
		contentPane.add(btnComicSansMs);

		// Schriftart "Courier New" Button
		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCourierNew_clicked();
			}
		});
		btnCourierNew.setBounds(309, 193, 117, 21);
		contentPane.add(btnCourierNew);

		// Textfeld zum Eingeben erstellt
		txtTextEingabe = new JTextField();
		txtTextEingabe.setText("Hier bitte Text eingeben");
		txtTextEingabe.setBounds(12, 224, 414, 27);
		contentPane.add(txtTextEingabe);
		txtTextEingabe.setColumns(10);

		// "ins Label schreiben" Button erstellt
		JButton btnLabel = new JButton("Ins Label schreiben");
		btnLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonInsLabel_clicked();
			}
		});
		btnLabel.setBackground(UIManager.getColor("Button.background"));
		btnLabel.setBounds(10, 261, 194, 21);
		contentPane.add(btnLabel);

		// Button "Text im Label l�schen" erstellt
		JButton btnLabelLeeren = new JButton("Text im Label l\u00F6schen");
		btnLabelLeeren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonTextloeschen_clicked();
			}
		});
		btnLabelLeeren.setBackground(UIManager.getColor("Button.background"));
		btnLabelLeeren.setBounds(228, 261, 198, 21);
		contentPane.add(btnLabelLeeren);

		// Text "Aufgabe3" als Label erstellt
		JLabel lblAufgabe3 = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblAufgabe3.setBounds(10, 292, 194, 13);
		contentPane.add(lblAufgabe3);

		// "rote Schrift" als Button erstellt
		JButton btnRedSchrift = new JButton("Rot");
		btnRedSchrift.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonRotSchrift_Clicked();
			}
		});
		btnRedSchrift.setBackground(UIManager.getColor("Button.background"));
		btnRedSchrift.setBounds(10, 314, 117, 21);
		contentPane.add(btnRedSchrift);

		// "blaue Schrift" als Button erstellt
		JButton btnBlueSchrift = new JButton("Blau");
		btnBlueSchrift.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonBlauSchrift_Clicked();
			}
		});
		btnBlueSchrift.setBackground(UIManager.getColor("Button.background"));
		btnBlueSchrift.setBounds(137, 314, 162, 21);
		contentPane.add(btnBlueSchrift);

		// "schwarze Schrift" als Button erstellt
		JButton btnBlackSchrift = new JButton("Schwarz");
		btnBlackSchrift.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonSchwarzSchrift_Clicked();
			}
		});
		btnBlackSchrift.setBackground(UIManager.getColor("Button.background"));
		btnBlackSchrift.setBounds(309, 314, 117, 21);
		contentPane.add(btnBlackSchrift);

		// Text "Aufgabe4" als Label erstellt
		JLabel lblAufgabe4 = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		lblAufgabe4.setBounds(10, 345, 317, 13);
		contentPane.add(lblAufgabe4);

		// "Schrift gr��er" als Button
		JButton btnSchriftgroesser = new JButton("+");
		btnSchriftgroesser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonSchriftgross_clicked();
			}
		});
		btnSchriftgroesser.setBounds(10, 368, 194, 21);
		contentPane.add(btnSchriftgroesser);

		// "Schrift kleiner" als Button
		JButton btnSchriftkleiner = new JButton("-");
		btnSchriftkleiner.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonSchriftklein_clicked();
			}
		});
		btnSchriftkleiner.setBounds(228, 368, 198, 21);
		contentPane.add(btnSchriftkleiner);

		// Text "Aufgabe5" als Label erstellt
		JLabel lblAufgabe5 = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabe5.setBounds(10, 403, 194, 13);
		contentPane.add(lblAufgabe5);

		// Button "linksb�ndig"
		JButton btnLinksbuending = new JButton("linksb\u00FCndig");
		btnLinksbuending.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonLinks_clicked();
			}
		});
		btnLinksbuending.setBackground(UIManager.getColor("Button.background"));
		btnLinksbuending.setBounds(10, 426, 117, 21);
		contentPane.add(btnLinksbuending);

		// Button "rechtsb�ndig"
		JButton btnRechtsbuendig = new JButton("rechtsb\u00FCndig");
		btnRechtsbuendig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonRechts_clicked();
			}
		});
		btnRechtsbuendig.setBackground(UIManager.getColor("Button.background"));
		btnRechtsbuendig.setBounds(309, 426, 117, 21);
		contentPane.add(btnRechtsbuendig);

		// Button "zentriert"
		JButton btnZentriert = new JButton("zentriert");
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonZentriert_clicked();
			}
		});
		btnZentriert.setBackground(UIManager.getColor("Button.background"));
		btnZentriert.setBounds(137, 426, 162, 21);
		contentPane.add(btnZentriert);

		// Text "Aufgabe6" als Label erstellt
		JLabel lblAufgabe6 = new JLabel("Aufgabe 6: Programm beenden");
		lblAufgabe6.setBounds(10, 457, 194, 13);
		contentPane.add(lblAufgabe6);

		// Button "EXIT" erstellt
		JButton btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonExit_clicked();
			}
		});
		btnExit.setBounds(10, 480, 416, 49);
		contentPane.add(btnExit);

		// Text oben erstellt
		txtTextZuVeraendern = new JTextField();
		txtTextZuVeraendern.setEditable(false);
		txtTextZuVeraendern.setHorizontalAlignment(SwingConstants.CENTER);
		txtTextZuVeraendern.setBackground(SystemColor.control);
		txtTextZuVeraendern.setText("Dieser Text soll ver\u00E4ndert werden. ");
		txtTextZuVeraendern.setBounds(10, 20, 416, 55);
		contentPane.add(txtTextZuVeraendern);
		txtTextZuVeraendern.setColumns(10);
	}

	public void buttonLinks_clicked() {
		txtTextZuVeraendern.setHorizontalAlignment(SwingConstants.LEFT);

	}

	public void buttonZentriert_clicked() {
		txtTextZuVeraendern.setHorizontalAlignment(SwingConstants.CENTER);
	}

	public void buttonRechts_clicked() {
		txtTextZuVeraendern.setHorizontalAlignment(SwingConstants.RIGHT);
	}

	public void buttonSchriftgross_clicked() {
		int groesse = txtTextZuVeraendern.getFont().getSize();
		String schriftartString = txtTextZuVeraendern.getFont().getFamily();

		txtTextZuVeraendern.setFont(new Font("Arial", Font.PLAIN, groesse + 1));

		if (schriftartString.equals("Comic Sans MS")) {
			txtTextZuVeraendern.setFont(new Font("Comic Sans MS", Font.PLAIN, groesse + 1));
		}
		if (schriftartString.equals("Courier New")) {
			txtTextZuVeraendern.setFont(new Font("Courier New", Font.PLAIN, groesse + 1));

		}
	}

	public void buttonSchriftklein_clicked() {
		int groesse = txtTextZuVeraendern.getFont().getSize();
		String schriftartString = txtTextZuVeraendern.getFont().getFamily();

		txtTextZuVeraendern.setFont(new Font("Arial", Font.PLAIN, groesse - 1));

		if (schriftartString.equals("Comic Sans MS")) {
			txtTextZuVeraendern.setFont(new Font("Comic Sans MS", Font.PLAIN, groesse - 1));
		}
		if (schriftartString.equals("Courier New")) {
			txtTextZuVeraendern.setFont(new Font("Courier New", Font.PLAIN, groesse - 1));

		}
	}

	public void buttonRotSchrift_Clicked() {
		txtTextZuVeraendern.setForeground(Color.RED);
	}

	public void buttonBlauSchrift_Clicked() {
		txtTextZuVeraendern.setForeground(Color.BLUE);
	}

	public void buttonSchwarzSchrift_Clicked() {
		txtTextZuVeraendern.setForeground(Color.BLACK);
	}

	public void buttonTextloeschen_clicked() {
		txtTextZuVeraendern.setText("");

	}

	public void buttonInsLabel_clicked() {
		txtTextZuVeraendern.setText(txtTextEingabe.getText());

	}

	public void buttonArial_clicked() {
		// txtTextEingabe.setText(txtTextEingabe.getText());
		txtTextZuVeraendern.setFont(new Font("Arial", Font.PLAIN, 12));

	}

	public void buttonComicSansMS_clicked() {
		// txtTextEingabe.setText(txtTextEingabe.getText());
		txtTextZuVeraendern.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));

	}

	public void buttonCourierNew_clicked() {
		// txtTextEingabe.setText(txtTextEingabe.getText());
		txtTextZuVeraendern.setFont(new Font("Courier New", Font.PLAIN, 12));

	}

	public void buttonExit_clicked() {
		System.exit(1);
	}

	public void buttonRed_clicked() {
		this.contentPane.setBackground(Color.RED);
	}

	public void buttonGreen_clicked() {
		this.contentPane.setBackground(Color.GREEN);
	}

	public void buttonBlue_clicked() {
		this.contentPane.setBackground(Color.BLUE);
	}

	public void buttonYellow_clicked() {
		this.contentPane.setBackground(Color.YELLOW);
	}

	public void buttonStandard_clicked() {
		this.contentPane.setBackground(SystemColor.menu);
	}

	public void buttonFarbewaehlen_clicked() {
		System.exit(1);
	}

}
